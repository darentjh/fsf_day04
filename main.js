/**
 * Created by Daren on 31/3/16.
 */

//load express
//"require" loads express. it loads the express library
//require("express") is standard cos we want to load express 
var express = require("express");

//we are creating an application
//() means im calling the library from above to create the application
// express here is just a name but we insert a meaningful name so we label it express as well
var app = express();

// we are going to tell express that public is our document root
// .static means that static resources will be served from here
// "public" is a relative path that we are adding to __dirname(absolute path) to make whole thing absolute
// static means files that dont change ie images
// dynamic files are files that keep changing ie straitstimes website, everyday new content
app.use(
    express.static(__dirname + "/public")
);

//now we are going to start a server, first we need a port
// port number should be any number above 1024; below 1024 need admin pass
// start server to listen on port 3000
// the function part is just to tell the console to print this out, so that we know it has started 
// you can simply put app.listen(3000); but you want the console to let you know it has started
app.listen(3000, function(){
    console.info("Application has started / is listening on port 3000");
});